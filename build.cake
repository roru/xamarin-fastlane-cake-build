var target = Argument("target", "Default");
var version = Argument("version", "0.0.0.0");
var configuration = Argument("configuration", "Release");

#addin "Cake.Xamarin"
#addin "Cake.FileHelpers"
#addin "Cake.Plist" 
#addin "Cake.AndroidAppManifest"

String Combine(params String[] parts)
{
    return System.IO.Path.Combine(parts);
}


var pathRoot = "./src";
var pathArtifacts = Combine(pathRoot, "artifacts");
var pathProjIOS = Combine(pathRoot, "iOS");
var pathProjDroid = Combine(pathRoot, "Droid");

var fileSln = File(Combine(pathRoot, "Build.sln"));
var fileComAss = File(Combine(pathRoot, "CommonAssemblyInfo.cs"));

var fileProjectIOS = File(Combine(pathProjIOS, "Build.iOS.csproj"));
var filePlistIOS = File(Combine(pathProjIOS, "Info.plist"));

var fileProjectDroid = File(Combine(pathProjDroid, "Build.Droid.csproj"));
var fileManifestDroid = File(Combine(pathProjDroid, "Properties", "AndroidManifest.xml"));

var epoch = (long)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;

Task("Clear")
    .Does(() => 
    {
        DeleteDirectory(pathArtifacts, true);
        MSBuild(fileSln, s => {
            s.SetConfiguration(configuration)
                .WithTarget("Clean");
        });
    });

Task("RestorePackage")
    .Does(() =>
    {
        NuGetRestore(fileSln);
    });

Task("UpdateAssamblyInfo")
    .Does(() =>
    {
        CreateAssemblyInfo(fileComAss, new AssemblyInfoSettings() {
            Product = "Roman Rudyak",
            Version = version,
            FileVersion = version,
            InformationalVersion = version,
            Copyright = "Copyright (c) Roman Rudyak"
        });
    });

Task("UpdatePlist")
    .Does(() =>
    {
        dynamic plist = DeserializePlist(filePlistIOS);

        plist["CFBundleShortVersionString"] = version;
        plist["CFBundleVersion"] = version;

        SerializePlist(filePlistIOS, plist);
    });

Task("UpdateAndroidManifest")
    .Does(() =>
    {
        var manifest = DeserializeAppManifest(fileManifestDroid);
        manifest.VersionName = version;
        manifest.VersionCode = Int32.Parse(version.Replace(".", string.Empty) + epoch.ToString().Substring(epoch.ToString().Length - 6));

        SerializeAppManifest(fileManifestDroid, manifest);
    });

Task("Build")
    .Does(() =>
    {
        // https://ghuntley.com/archive/2016/07/11/example-of-xamarin-ios-with-cake/
        MSBuild(fileProjectIOS, settings =>
        {
            settings.SetConfiguration("Debug")
                    .WithTarget("Rebuild")
                    //.WithProperty("BuildIpa", "True")
                    .WithProperty("ArchiveOnBuild", "True")
                    .WithProperty("OutputPath", "bin/iPhoneSimulator/")
                    .WithProperty("Platform", "iPhoneSimulator");
        });

        // https://ghuntley.com/archive/2016/07/11/example-of-xamarin-android-with-cake/
        MSBuild(fileProjectDroid, settings =>
            settings.SetConfiguration("Debug")
                .WithTarget("SignAndroidPackage")
                .WithProperty("DebugSymbols", "true")
                .WithProperty("DebugType", "Full")
                .WithProperty("OutputPath", "bin/Debug/"));


        // uncomment when distribution provision will be installed
        // MSBuild(fileProjectIOS, settings =>
        // {
        //     settings.SetConfiguration("Release")
        //             .WithTarget("Rebuild")
        //             .WithProperty("Platform", "iPhone");
        // });
    });

Task("MakeArtifacts")
    .IsDependentOn("Build")
    .Does(() =>
    {
        EnsureDirectoryExists(pathArtifacts);
        var files = GetFiles(Combine(fileProjectIOS, "bin/**/*.ipa"));
        CopyFiles(files, pathArtifacts);
    });

Task("Default")
     .IsDependentOn("Clear")
     .IsDependentOn("UpdateAssamblyInfo")
     .IsDependentOn("UpdatePlist")
     .IsDependentOn("UpdateAndroidManifest")
     .IsDependentOn("RestorePackage")
     .IsDependentOn("Build")
     .Does(() => {});

Task("Test")
    .Does(()=>Information("Test Task"));

RunTarget(target);